package demo.model;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyBigNumber {
	private String number1;
	private String number2;

	public MyBigNumber() {
		super();
	}

	public MyBigNumber(String number1, String number2) {
		super();
		this.number1 = number1;
		this.number2 = number2;
	}

	public MyBigNumber(String number1) {
		super();
		this.number1 = number1;
	}

	public String getNumber1() {
		return number1;
	}

	public void setNumber1(String number1) {
		this.number1 = number1;
	}

	public String getNumber2() {
		return number2;
	}

	public void setNumber2(String number2) {
		this.number2 = number2;
	}

	@Override
	public String toString() {
		return "MyBigNumber [number1=" + number1 + ", number2=" + number2 + "]";
	}

//input string
	public void input() {
		Scanner scanner = new Scanner(System.in);
		String num1;
		String num2;
		System.out.println("Input two numbers:");
		System.out.println("Input number one:");
		num1 = scanner.next();
		System.out.println("Input number two:");
		num2 = scanner.next();
		System.out.println();
		
//sum two number		
		String sumTwoNumber = sum(num1,num2);
		System.out.println("Sum two numbers:" + sumTwoNumber);
		
		scanner.close();
	}

//reverse string
	public String reverse(String string) {
		String stringReverse = "";
		for (int i = 0; i < string.length(); i++) {
			stringReverse += string.charAt(string.length() - 1 - i);
		}
		return (stringReverse);
	}

//max string length
	public Integer max(int num1, int num2) {
		int max;
		if (num1 > num2) {
			max = num1;
		} else {
			max = num2;
		}
		return max;
	}

//sum two number	
	public String sum(String num1, String num2) {
		Logger diary = Logger.getLogger("Diary1");
		diary.setLevel(Level.ALL);
		
		String string = "";
		int lengthNum1 = num1.length();
		int lengthNum2 = num2.length();
		int maxLength = max(lengthNum1, lengthNum2);
		num1 = reverse(num1);
		num2 = reverse(num2);

//add 0 to the string		
		if(maxLength >lengthNum1) {
			for(int i = maxLength; i >= lengthNum1; i--) {
				num1 += "0";
			}
		}
		if(maxLength >lengthNum2) {
			for(int i = lengthNum2; i >= lengthNum2; i--) {
				num2 += "0";
			}
		}
		
		int numberMemory = 0;		
		for(int i = 0; i< maxLength; i++) {
			int sumTwoNumber = 0;
			sumTwoNumber = num1.charAt(i) -'0' + num2.charAt(i) - '0' + numberMemory;
			diary.log(Level.INFO,"Step 1: Sum two number at position " + i + " : "  + (num1.charAt(i) -'0') + " " + (num2.charAt(i) -'0') + " " + numberMemory + " = " + sumTwoNumber);
			
			diary.log(Level.INFO,"Step 2: Add " + (sumTwoNumber % 10) + " to string " + string );
			string += sumTwoNumber % 10;
			numberMemory = sumTwoNumber / 10;	
		}
		if(numberMemory == 1) {
			string += 1;
		}
		
		string = reverse(string);
		diary.log(Level.INFO,"Step 3: Reverse string " + string);
		
		return string;		
	}
}
